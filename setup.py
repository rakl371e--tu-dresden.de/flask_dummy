# -*- coding: utf-8 -*-
# Copyright (C) 2024 ralf.klammer@tu-dresden.de

from setuptools import setup

setup(
    name="flask_dummy",
    version="0.0.1",
    description="Simple starting point for a flask based project",
    author="Ralf Klammer",
    author_email="ralf.klammer@tu-dresden.de",
    packages=["project_code", "project_app"],
    install_requires=[
        "click",
        "redis",
        "flask",
        "flask_json",
    ],
    include_package_data=True,
    entry_points={
        "console_scripts": [
            "project_code = project_code.cli:main",
            "project_app = project_app.app:startup",
        ]
    },
)
