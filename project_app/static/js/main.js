
get_json_via_post = function(e) {
    e.preventDefault();
    $.ajax({
        url: '/json_response',
        method: 'POST',
        dataType: 'json',
        data: {'key': 'value'},
        success: function(response) {
            console.log(response)
        }
    });
}

$(document).ready(function() {

    $('.get_json').click(get_json_via_post)

    console.log('main.js has been loaded! ') // OK
})
