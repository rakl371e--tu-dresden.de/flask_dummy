# -*- coding: utf-8 -*-
# Copyright (C) 2024 ralf.klammer@tu-dresden.de
import logging

import redis


from flask import Flask, render_template, request, make_response
from flask_json import json_response, FlaskJSON

from project_code.util import config

log = logging.getLogger(__name__)


# initialize flask
app = Flask(__name__)
FlaskJSON(app)


cache = redis.Redis(host="redis", port=6379)


@app.context_processor
def inject_global_vars():
    return {"title": "Workshop Planner"}


@app.route("/")
def main():
    return render_template("main.html")


@app.route("/json_response", methods=["GET"])
def get_json_via_get():
    args = request.args
    print(args)
    return json_response(value="OK", data={})


@app.route("/json_response", methods=["POST"])
def get_json_via_post():
    args = request.args
    print(args)
    return json_response(value="OK", data={})


@app.route("/download_csv", methods=["GET"])
def download_csv():
    input_value = int(request.args.get("input_value", default=10))
    csv_data = "i,i*2, pow(i, 2)\n"
    for i in range(input_value):
        csv_data += "%s,%s,%s\n" % (i, i * 2, pow(i, 2))

    response = make_response(csv_data)
    response.headers["Content-Type"] = "text/csv"
    response.headers["Content-Disposition"] = "attachment; filename=test.csv"

    return response


def startup():
    app.run(
        host=config("main", "host", default="0.0.0.0"),
        port=config("main", "port", default=8077),
        debug=config("log", "level", default="DEBUG") == "DEBUG",
    )
