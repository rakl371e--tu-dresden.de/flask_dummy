# -*- coding: utf-8 -*-
# Copyright (C) 2024 ralf.klammer@tu-dresden.de
import logging

import click
from .util import cli_startup

log = logging.getLogger(__name__)


@click.group()
@click.option("--debug/--no-debug", "-d", is_flag=True, default=False)
@click.pass_context
def main(ctx, debug):
    cli_startup(log_level=debug and logging.DEBUG or logging.INFO)
    ctx.ensure_object(dict)
    ctx.obj["DEBUG"] = debug


@main.command()
@click.pass_context
def testing(ctx):
    click.echo(f"Debug is {'on' if ctx.obj['DEBUG'] else 'off'}")


if __name__ == "__main__":
    main(obj={})
