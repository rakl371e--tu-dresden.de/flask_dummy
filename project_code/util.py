# -*- coding: utf-8 -*-
# Copyright (C) 2024 ralf.klammer@tu-dresden.de
import logging
from configparser import ConfigParser

log = logging.getLogger(__name__)


def config(section, parameter, default=None):
    _config = ConfigParser()
    _config.read("config.ini")

    if section not in _config:
        log.warn("Section: %s not in *.ini -> using default" % section)
        return default
    config_val = _config[section].get(parameter)
    if not config_val:
        log.warn(
            "Parameter: %s not in section: %s of *.ini -> using default"
            % (parameter, section)
        )
        return default
    else:
        return config_val


def cli_startup(log_level=logging.INFO, log_file=None):
    log_config = dict(
        level=log_level,
        format="%(asctime)s %(name)-10s %(levelname)-4s %(message)s",
    )
    if log_file:
        log_config["filename"] = log_file

    logging.basicConfig(**log_config)
    logging.getLogger("").setLevel(log_level)
